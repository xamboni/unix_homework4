Should I handle bad input or bad array input or both?

In the multithreaded application if the second program is just
reading the memory do I still need to use semaphors?

  Use some kind of protection. This could be a semaphor.

Answer: Catching signals in install_and_monitor.
  Reinitialize data on SIGTERM
  Just kill the threads on SIGHUP or call exit(0); There is no shared
  memory to clean up anyways.

Design of sighup handler.
