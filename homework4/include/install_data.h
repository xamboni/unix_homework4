
/*---------------------------------------------------------------------
 *
 * install_data.h
 *
 *
 * Alexander Morgan
 * amorga36
 * Unix Programming Fall 2017
 * Shared Memory Homework 4
 * Description: Install data and install* program deps. 
 *
 *
 ---------------------------------------------------------------------*/

#ifndef INSTALL_DATA_H
#define INSTALL_DATA_H

/*---------------------------- Includes ------------------------------*/

/* Standard */

/* User */

/*----------------------------- Globals ------------------------------*/

/* Key so both programs can access shared memory. */
int key;
const char* key_path = "/tmp";

/*------------------------------ Types -------------------------------*/

#define LOCAL_DEBUG_EN 0
#define ARRAY_SIZE 20

typedef struct 
{ 
    int is_valid;
    float x;
    float y;
} memory;

/*---------------------------- Constants -----------------------------*/

/*--------------------------- Prototypes -----------------------------*/

#endif
