
/*---------------------------------------------------------------------
 *
 * shared_mem.h
 *
 *
 * Alexander Morgan
 * amorga36
 * Unix Programming Fall 2017
 * Shared Memory Homework 4
 * Description: Library helper for shared memory. 
 *
 *
 ---------------------------------------------------------------------*/

#ifndef SHARED_MEM_H
#define SHARED_MEM_H

/*---------------------------- Includes ------------------------------*/

/* Standard */

/* User */

/*----------------------------- Globals ------------------------------*/

/*------------------------------ Types -------------------------------*/

/*---------------------------- Constants -----------------------------*/

#define OK 0
#define ERROR -1

#define ARRAY_SIZE 20
#define MALLOC_SIZE (ARRAY_SIZE*sizeof(memory))
#define SHM_MODE 0600

/*--------------------------- Prototypes -----------------------------*/

void* connect_shm(int key, int size);
int detach_shm(void* addr);
int destroy_shm(int key);

#endif
