/*---------------------------------------------------------------------
 *
 * install_and_monitor.c
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Unix Programming Fall 2017
 * Shared Memory Homework 3
 * 
 *
 ---------------------------------------------------------------------*/

/*---------------------------- Includes ------------------------------*/

/* Standard */
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <semaphore.h>
#include <errno.h>
#include <sched.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>

/* User */
#include "log_mgr.h"
#include "install_and_monitor.h"
#include "install_data.h"
#include "shared_mem.h"

/*----------------------------- Globals ------------------------------*/

memory shared_memory[20];
static sem_t sem1;
static sem_t sem2;
const char* input_file = "test.txt";
static pthread_mutex_t C_mutex;
static FILE* fp = NULL;
unsigned int MonitorAmount = 30;

/*------------------------------ Types -------------------------------*/

typedef struct
{
        sem_t *first_sem;
        sem_t *second_sem;
        char *msg;
} THREAD_DATA;

/*---------------------------- Constants -----------------------------*/

/*--------------------------- Prototypes -----------------------------*/

void* install_data( void* ptr );
void* monitor_shim( void* ptr );
void process_file(FILE* input_file, memory* shared_memory, pthread_mutex_t C_mutex);
void check_shared_memory(unsigned int program_runtime, memory* shared_memory, pthread_mutex_t C_mutex_loc);

/*------------------------- Implementation ---------------------------*/

/* Clear the shared memory region and reinstall original data. */
void catchSigHup(int placeHolder)
{
    // TODO: This needs to be handled but Im not sure how.
    unsigned int i = 0;

    log_event(INFO, "SigHup caught: Shared memory and file parsing reset.");

    /* Clear the shared memory. */
    for(i = 0; i < ARRAY_SIZE; i += 1)
    {
        shared_memory[i].is_valid = 0;
        shared_memory[i].x = 0;
        shared_memory[i].y = 0;
    }

    /* Start a call to process the file again here. */
    fseek(fp, 0, SEEK_SET);
    process_file(fp, shared_memory, C_mutex);

    fclose(fp);

    /* Clean up and destroy shared memory. */
    destroy_shm(key);
    exit(0);
}

/* Detach and destroy shared memory then exit. */
void catchSigTerm(int placeHolder)
{
    /* Originally we detached and destoryed shared memory here.
     * For this program there is no shared memory. We can either
     * wait for threading to play out or kill them manually.
     * Here we are just exiting cleanly and letting threads run
     * their course.
     */
    exit(OK);
}

/*
 * Inputs:
 * Outputs:
 * Note: 
 * 
----------------------------------------------------------------------*/

int main ( int argc, char* argv[] )
{
    int rtn_val;
    pthread_t thread1;
    pthread_t thread2;
    THREAD_DATA init1;
    THREAD_DATA init2;

    /* first semaphore starts off unlocked */
    sem_init (&sem1, 0, 1);
    /* second semaphore starts off locked */
    sem_init (&sem2, 0, 0);

    init1.first_sem = &sem1;
    init1.second_sem = &sem2;
    init1.msg = "Hlowrd";

    init2.first_sem = &sem2;
    init2.second_sem = &sem1;
    init2.msg = "el ol\n";

    /* Install data thread. */
    if ((rtn_val = pthread_create( &thread1, NULL,
                install_data, (void*) &init1)) != 0)
    {
        printf("pthread error.\n");
        perror("pthread_create1: Error.");
    }

    /* Monitor thread. */
    if ((rtn_val = pthread_create(&thread2, NULL,
                monitor_shim, (void*) &init2)) != 0)
    {
        printf("pthread error.\n");
        perror("pthread_create2: Error.");
    }

    /* Mutex which arbitrates memory accesses. */
    if ((rtn_val = pthread_mutex_init(&C_mutex, NULL)) != 0)
    {
        printf("mutex error.\n");
        perror("mutex init: Error.");
    }

    pthread_join (thread1, NULL);
    pthread_join (thread2, NULL);
    exit(0);
}

void* install_data( void* ptr )
{
    FILE* fp = NULL;

    /* Check if file exists and is readable. */
    if( access( input_file, F_OK | R_OK ) != -1 )
    {
        if ( LOCAL_DEBUG_EN )
        {
            printf("Key: 0x%x, Shared mem: %p.\n", key, shared_memory);
        }

        /* Register sig handlers to fix memory on interrupt. */
        signal(SIGHUP, catchSigHup);
        signal(SIGTERM, catchSigTerm);

        fp = fopen(input_file, "r");
        if (fp == NULL)
            exit(EXIT_FAILURE);

        process_file(fp, shared_memory, C_mutex);
    }

    else
    {
        printf("Are you an idiot Xander? ... Yes.\n");
    }
}

void* monitor_shim( void* ptr)
{
    unsigned int program_runtime = 0;

    while(program_runtime < MonitorAmount)
    {
        check_shared_memory(program_runtime, shared_memory, C_mutex);
        sleep(1);
        program_runtime += 1;
    }
}


void process_file(FILE* input_file, memory* shared_memory_loc, pthread_mutex_t C_mutex_loc)
{
    char* line = NULL;
    size_t len = 0;
    ssize_t read;

    while ((read = getline(&line, &len, input_file)) != -1)
    {
        char* token;
        char* argv1_array = NULL;

        unsigned int i = 0;
        unsigned int j = 0;

        unsigned int index = 0;
        float x = 0;
        float y = 0;
        int delay = 0;

        argv1_array = (char*) malloc(strlen(line));
        strcpy(argv1_array, line);

        token = strtok (argv1_array, " ");

        for (i = 0; i < 4; i += 1)
        {

            /* Handle index from file. */
            if (0 == i)
            {
                /* TODO: Handle bad return. */
                index = atoi (token);
            }

            /* Handle x from file. */
            else if (1 == i)
            {
                /* TODO: Handle bad return. */
                x = atoi (token);
            }

            /* Handle y from file. */
            else if (2 == i)
            {
                /* TODO: Handle bad return. */
                y = atoi (token);
            }

            /* Handle delay from file. */
            else if (3 == i)
            {
                /* TODO: Handle bad return. */
                delay = atoi (token);
            }

            token = strtok(NULL, " \t");
        }

        if ( LOCAL_DEBUG_EN )
        {
            printf("%d %f %f %d\n", index, x, y, delay);
        }

        /* Process the line here. First handle the delay. */
        if ( LOCAL_DEBUG_EN )
        {
            printf("Delaying for %d seconds.\n", abs(delay));
        }
        sleep( abs(delay) );

        /* TODO: Check the array index is between 0 and 19. */
        /* Check if we are assigning shared memory or invalidating it. */
        if( delay < 0)
        {
            if (pthread_mutex_lock (&C_mutex_loc) != 0)
            {
                perror("pthread_mutex_lock failed\n");
            }

            shared_memory_loc[index].is_valid = 0;

            if (pthread_mutex_unlock (&C_mutex_loc) != 0)
            {
                perror("pthread_mutex_unlock failed\n");
            }
        }

        /* Handle assignments of x and y. */
        else
        {
            if (pthread_mutex_lock (&C_mutex_loc) != 0)
            {
                perror("pthread_mutex_lock failed\n");
            }

            shared_memory_loc[index].is_valid = 1;
            shared_memory_loc[index].x = x;
            shared_memory_loc[index].y = y;

            if (pthread_mutex_unlock (&C_mutex_loc) != 0)
            {
                perror("pthread_mutex_unlock failed\n");
            }
        }
    }

    if (line)
        free(line);
}

void check_shared_memory(unsigned int program_runtime, memory* shared_memory, pthread_mutex_t C_mutex_loc)
{
    unsigned int i = 0;

    unsigned int active_elements = 0;
    float average_x = 0.0;
    float average_y = 0.0;

    for(i = 0; i < 20; i += 1)
    {
        if (pthread_mutex_lock (&C_mutex_loc) != 0)
        {
            perror("pthread_mutex_lock failed\n");
        }

        if (shared_memory[i].is_valid != 0)
        {
            active_elements += 1;
            average_x += shared_memory[i].x;
            average_y += shared_memory[i].y;
        }

        if (pthread_mutex_unlock (&C_mutex_loc) != 0)
        {
            perror("pthread_mutex_unlock failed\n");
        }
    }

    if (active_elements > 0)
    {
        average_x /= active_elements;
        average_y /= active_elements;
    }

    printf("At time %d:%d elements are active:x=%f and y=%f\n",
               program_runtime, active_elements, average_x, average_y);
}
