
/*---------------------------------------------------------------------
 *
 * install_and_monitor.h
 *
 *
 * Alexander Morgan
 * amorga36
 * Unix Programming Fall 2017
 * Logging Homework 2
 * Description: Header file for install_and_monitor program. 
 *
 *
 ---------------------------------------------------------------------*/

#ifndef INSTALL_AND_MONITOR_H
#define INSTALL_AND_MONITOR_H

/*---------------------------- Includes ------------------------------*/

/* Standard */

/* User */

/*----------------------------- Globals ------------------------------*/

/*------------------------------ Types -------------------------------*/

/*---------------------------- Constants -----------------------------*/

/*--------------------------- Prototypes -----------------------------*/

#endif
