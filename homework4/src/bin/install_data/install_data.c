/*---------------------------------------------------------------------
 *
 * install_data.c
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Unix Programming Fall 2017
 * Shared Memory Homework 4
 * 
 *
 ---------------------------------------------------------------------*/

/*---------------------------- Includes ------------------------------*/

/* Standard */
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <sys/shm.h>

/* User */
#include "log_mgr.h"
#include "shared_mem.h"
#include "install_data.h"

/*----------------------------- Globals ------------------------------*/

FILE* fp = NULL;

/*------------------------------ Types -------------------------------*/

/*---------------------------- Constants -----------------------------*/

/*--------------------------- Prototypes -----------------------------*/

void process_file(FILE* input_file, memory* shared_memory);
void clean_up(void);

/*------------------------- Implementation ---------------------------*/

/* Clear the shared memory region and reinstall original data. */
void catchSigHup(int placeHolder)
{
    memory* shared_memory = NULL;
    unsigned int i = 0;

    log_event(INFO, "SigHup caught: Shared memory and file parsing reset.");

    /* Clear the shared memory. */
    shared_memory = (memory*) connect_shm(key, sizeof(memory) * ARRAY_SIZE);

    for(i = 0; i < ARRAY_SIZE; i += 1)
    {
        shared_memory[i].is_valid = 0;
        shared_memory[i].x = 0;
        shared_memory[i].y = 0;
    }

    /* Start a call to process the file again here. */
    fseek(fp, 0, SEEK_SET);
    process_file(fp, shared_memory);

    fclose(fp);

    /* Clean up and destroy shared memory. */
    destroy_shm(key);
    exit(OK);
}

/* Detach and destroy shared memory then exit. */
void catchSigTerm(int placeHolder)
{
    key = ftok(key_path, 'S');
    memory* shared_memory = NULL;

    /* Initialize array of 20 objects in shared mem. */
    shared_memory = (memory*) connect_shm(key, sizeof(memory) * 20);

    if ( LOCAL_DEBUG_EN )
    {
        printf("Key: 0x%x, Shared mem: %p.\n", key, shared_memory);
        printf("SigTerm caught. Detaching shared memory.\n");
    }
    detach_shm(shared_memory);

    if ( LOCAL_DEBUG_EN )
    {
        printf("Destroying shared memory.\n");
    }
    destroy_shm(key);

    log_event(INFO, "SigTerm caught. Shared memory detached and destroyed.");
    exit(ERROR);
}

/*----------------------------------------------------------------------
 *
 * Inputs: File path.
 * Outputs: 
 * Note: 
 * 
----------------------------------------------------------------------*/

int main ( int argc, char* argv[] )
{

    /* Check first argument for file string. */
    if (argc < 2)
    {
        printf("Not enough arguments supplied.\n");
    }

    else if (argc < 3)
    {
        if ( LOCAL_DEBUG_EN )
        {
            printf("Two arguments supplied.\n");
        }

        /* Check if file exists and is readable. */
        if( access( argv[1], F_OK | R_OK ) != -1 )
        {
            // file exists
            memory* shared_memory = NULL;

            //key = IPC_PRIVATE;
            key = ftok(key_path, 'S');
            shared_memory = NULL;

            /* Initialize array of 20 objects in shared mem. */
            shared_memory = (memory*) connect_shm(key, sizeof(memory) * 20);

            if ( LOCAL_DEBUG_EN )
            {
                printf("Key: 0x%x, Shared mem: %p.\n", key, shared_memory);
            }

            /* Register sig handlers to fix memory on interrupt. */
            signal(SIGHUP, catchSigHup);
            signal(SIGTERM, catchSigTerm);

            fp = fopen(argv[1], "r");
            if (fp == NULL)
                exit(EXIT_FAILURE);

            /* TODO: Handle bad input. */ 
            process_file(fp, shared_memory);
        
            fclose(fp);

            /* Clean up and destroy shared memory. */
            destroy_shm(key);
        }

        else
        {
            printf("File does not exist or is not readable.\n");
        }
    }

    else
    {
        printf("Catch all.\n");
    }
}

void process_file(FILE* input_file, memory* shared_memory)
{
    char* line = NULL;
    size_t len = 0;
    ssize_t read;

    while ((read = getline(&line, &len, input_file)) != -1)
    {
        char* token;
        char* argv1_array = NULL;

        unsigned int i = 0;
        unsigned int j = 0;

        unsigned int index = 0;
        float x = 0;
        float y = 0;
        int delay = 0;

        argv1_array = (char*) malloc(strlen(line));
        strcpy(argv1_array, line);

        token = strtok (argv1_array, " ");

        for (i = 0; i < 4; i += 1)
        {

            /* Handle index from file. */
            if (0 == i)
            {
                /* TODO: Handle bad return. */
                index = atoi (token);
            }

            /* Handle x from file. */
            else if (1 == i)
            {
                /* TODO: Handle bad return. */
                x = atoi (token);
            }

            /* Handle y from file. */
            else if (2 == i)
            {
                /* TODO: Handle bad return. */
                y = atoi (token);
            }

            /* Handle delay from file. */
            else if (3 == i)
            {
                /* TODO: Handle bad return. */
                delay = atoi (token);
            }

            token = strtok(NULL, " \t");
        }

        if ( LOCAL_DEBUG_EN )
        {
            printf("%d %f %f %d\n", index, x, y, delay);
        }

        /* Process the line here. First handle the delay. */
        if ( LOCAL_DEBUG_EN )
        {
            printf("Delaying for %d seconds.\n", abs(delay));
        }
        sleep( abs(delay) );

        /* TODO: Check the array index is between 0 and 19. */
        /* Check if we are assigning shared memory or invalidating it. */
        if( delay < 0)
        {
            shared_memory[index].is_valid = 0;
        }

        /* Handle assignments of x and y. */
        else
        {
            shared_memory[index].is_valid = 1;
            shared_memory[index].x = x;
            shared_memory[index].y = y;
        }
    }

    if (line)
        free(line);
}

void clean_up(void)
{

}
