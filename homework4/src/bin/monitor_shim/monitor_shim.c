/*---------------------------------------------------------------------
 *
 * monitor_shim.c
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Unix Programming Fall 2017
 * Monitor Shim Homework 4
 * 
 *
 ---------------------------------------------------------------------*/

/*---------------------------- Includes ------------------------------*/

/* Standard */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

/* User */
#include "log_mgr.h"
#include "monitor_shim.h"
#include "shared_mem.h"
#include "install_data.h"

/*----------------------------- Globals ------------------------------*/

/*------------------------------ Types -------------------------------*/

/*---------------------------- Constants -----------------------------*/

/*--------------------------- Prototypes -----------------------------*/

void print_memory(memory* shared_memory);
void check_shared_memory(unsigned int program_runtime, memory* shared_memory);

/*------------------------- Implementation ---------------------------*/

/*
 * Inputs:
 * Outputs:
 * Note: 
 * 
----------------------------------------------------------------------*/

int main ( int argc, char* argv[] )
{
    unsigned int program_runtime = 0;
    unsigned int monitor_amount = 30;
    memory* shared_memory = NULL;

    if (argc > 1)
    {
        /* TODO: Check the validity of the number. */
        monitor_amount = atoi (argv[1]);
    }

    /* Initialize array of 20 objects in shared mem. */
    key = ftok(key_path, 'S');
    shared_memory = (memory*) connect_shm(key, 0);

    if (shared_memory == (memory*) -1)
    {
        log_event(FATAL, "Error in accessing shared memory.");
        perror("Error in accessing shared memory.");
        exit(0);
    }

    while(program_runtime < monitor_amount)
    {
        check_shared_memory(program_runtime, shared_memory);
        sleep(1);
        program_runtime += 1;
    }

}

void check_shared_memory(unsigned int program_runtime, memory* shared_memory)
{
    unsigned int i = 0;

    unsigned int active_elements = 0;
    float average_x = 0.0;
    float average_y = 0.0;

    for(i = 0; i < 20; i += 1)
    {
        if (shared_memory[i].is_valid != 0)
        {
            active_elements += 1;
            average_x += shared_memory[i].x;
            average_y += shared_memory[i].y;
        }
    }

    if (active_elements > 0)
    {
        average_x /= active_elements;
        average_y /= active_elements;
    }

    printf("At time %d:%d elements are active:x=%f and y=%f\n",
               program_runtime, active_elements, average_x, average_y);
}

void print_memory(memory* shared_memory)
{
    unsigned int clean_i = 0;

    for (clean_i = 0; clean_i < 20; clean_i += 1)
    {
        printf("%d: %d %f %f.\n", clean_i, shared_memory[clean_i].is_valid, shared_memory[clean_i].x, shared_memory[clean_i].y);
    }
}

