
/*---------------------------------------------------------------------
 *
 * log_mgr.c
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Unix Programming Fall 2017
 * Logging Homework 2
 * Description: Library helper to create log files. 
 *
 ---------------------------------------------------------------------*/

/*---------------------------- Includes ------------------------------*/

/* Standard */
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdbool.h>
#include <time.h>
#include <string.h>
#include <sys/stat.h>

/* User */
#include "log_mgr.h"

/*----------------------------- Globals ------------------------------*/

static int LogfileDescriptor = -1;

/*------------------------------ Types -------------------------------*/

/*---------------------------- Constants -----------------------------*/

/*--------------------------- Prototypes -----------------------------*/

/*------------------------- Implementation ---------------------------*/

/*
 * Inputs: String to be logged to file.
 * Outputs: Formatted string and timestamp data.
 * Note:
 * 
----------------------------------------------------------------------*/

int log_event (Levels l, const char *fmt, ...)
{
    char buffer[256];
    char dateBuffer[64];
    va_list args;
    int date_length = 0;
    int buffer_length = 0;
    int write_length = 0;
    time_t t = 0;
    struct tm tm;

    if (-1 == LogfileDescriptor)
    {
        /* First instance of logfile. */
        if ( set_logfile(DEFAULT_LOGFILE) )
        {
            return ERROR;
        }
    }

    t = time(NULL);
    tm = *localtime(&t);


    va_start (args, fmt);
    buffer_length = vsprintf (buffer, fmt, args);

   /* Prepare the level. */
   if (0 == l)
   {
    date_length = sprintf (dateBuffer, "%d-%d-%d %d:%d:%d:INFO:", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
   }
   else if (1 == l)
   {
    date_length = sprintf (dateBuffer, "%d-%d-%d %d:%d:%d:WARNING:", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
   }
   else if (2 == l)
   {
    date_length = sprintf (dateBuffer, "%d-%d-%d %d:%d:%d:FATAL:", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
   }
   else
   {
       printf("Error while parsing level.\n");
       return ERROR;
   }

   /* Dynamic allocation of string concatenated buffer. */
   int newSize = date_length + buffer_length + 1; 

   char* newBuffer = (char *)malloc(newSize);

   strcpy(newBuffer, dateBuffer);
   strcat(newBuffer, buffer);
   strcat(newBuffer, "\n");

    /* Format data for log purposes and print to logfile. */
    write_length = write (LogfileDescriptor, newBuffer, newSize);
    va_end (args);

   free(newBuffer);

    if (write_length != newSize)
    {
        perror("Error while writing to log file.");
        return ERROR;
    }

    return SUCCESS;
}

int set_logfile (const char *logfile_name)
{
    int new_logfile_descriptor = 0;
    int ret = SUCCESS;

    mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
    new_logfile_descriptor = open(logfile_name, 'a', mode);

    // TODO: Use perror/errno to ascertain whether open succeeded.

    /* If new logfile is successful then close previous logfile. */
    if (new_logfile_descriptor)
    {
        if (LogfileDescriptor > 0)
        {
            close_logfile();
        }
        LogfileDescriptor = new_logfile_descriptor;

        ret = SUCCESS;
    }

    else
    {
         perror("Error while openeing new logfile.");
         ret = ERROR;
    }

    return ret;
}

void close_logfile (void)
{
    // TODO: Use perror and error here.
    if (-1 != LogfileDescriptor)
    {
        close(LogfileDescriptor);
        LogfileDescriptor = -1;
    }
}
