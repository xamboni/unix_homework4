
/*---------------------------------------------------------------------
 *
 * log_mgr.c
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Unix Programming Fall 2017
 * Logging Homework 2
 * Description: Library helper to create log files. 
 *
 ---------------------------------------------------------------------*/

/*---------------------------- Includes ------------------------------*/

/* Standard */
#include<stdio.h>
#include<sys/shm.h>
#include <errno.h>

/* User */
#include "shared_mem.h"
#include "log_mgr.h"

/*----------------------------- Globals ------------------------------*/

/*------------------------------ Types -------------------------------*/

/*---------------------------- Constants -----------------------------*/

/*--------------------------- Prototypes -----------------------------*/

/*------------------------- Implementation ---------------------------*/

/*
 * Inputs:
 * Outputs:
 * Note:
 * 
----------------------------------------------------------------------*/

void* connect_shm(int key, int size)
{
    int shmid = 0;
    char* shmptr = NULL;

    /* Call to shmget in order to allocate shared memory. */
    if ((shmid = shmget(key, size, IPC_CREAT | 0666)) < 0)
    {
        log_event(FATAL, "shmget error.");
        perror("shmget error.");
    }

    if ((shmptr = shmat(shmid, (void *)0, 0)) == (void*) -1)
    {
        log_event(FATAL, "shmat error.");
        perror("shmat error.");
    }
        
    return shmptr;
}

int detach_shm(void* addr)
{
    int return_val = shmdt(addr);;

    if ( return_val < 0)
    {
        log_event(FATAL, "shmdt error.");
        perror("shmdt error.");
        return ERROR;
    }

    else if (return_val == 0)
    {
        return OK;
    }

    else
    {
        log_event(FATAL, "Unknown shmdt error.");
        perror("Unknown shmdt error.");
        return ERROR;
    }
}

int destroy_shm(int key)
{
    int shmid = 0;

    if ((shmid = shmget(key, 0, 0666)) < 0)
    {
        log_event(FATAL, "shmget error.");
        perror("shmget error.");
        return ERROR;
    }

    if (shmctl(shmid, IPC_RMID, 0) < 0)
    {
        log_event(FATAL, "shmid error.");
        perror("shmid error.");
        return ERROR;
    }

    return OK;
}
